import {
  SET_PLAYERS,
  ADD_PLAYER,
  REMOVE_PLAYER
} from '@/store/mutationTypes.js'
import { Player as PlayerAPI } from '@/api/player.js'

const state = () => ({
  players: []
})

const actions = {
  async getPlayers ({ commit }) {
    try {
      const players = await PlayerAPI.getAll()
      commit(SET_PLAYERS, players)
    } catch (error) {
      throw new Error(`Error PlayerAPI.getPlayers: ${error}`)
    }
  },
  async getPlayer ({ commit }, { searchQuery }) {
    try {
      const player = await PlayerAPI.getPlayer({ searchQuery })
      return player
    } catch (error) {
      throw new Error(`Error PlayerAPI.getPlayer: ${error}`)
    }
  },
  async addPlayer ({ commit }, data) {
    try {
      const player = await PlayerAPI.storePlayer(data)
      commit(ADD_PLAYER, player)
    } catch (error) {
      throw new Error(`Error PlayerAPI.addPlayer: ${error}`)
    }
  },
  async removePlayer ({ commit }, { idPlayer }) {
    try {
      await PlayerAPI.deletePlayer({ idPlayer })
      commit(REMOVE_PLAYER, idPlayer)
    } catch (error) {
      throw new Error(`Error PlayerAPI.removePlayer: ${error}`)
    }
  }
}

const mutations = {
  [SET_PLAYERS] (state, players) {
    state.players = players
  },
  [ADD_PLAYER] (state, player) {
    state.players.push(player)
  },
  [REMOVE_PLAYER] (state, idPlayer) {
    const playerIndex = state.players.findIndex(player => player.idPlayer === idPlayer)
    if (playerIndex < 0) return
    state.players.splice(playerIndex, 1)
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
