import Vue from 'vue'
import Vuex from 'vuex'
import players from '@/store/modules/players'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    players
  }
})

export default store

// export default new Vuex.Store({
//   state: {
//   },
//   getters: {
//   },
//   mutations: {
//   },
//   actions: {
//   },
//   modules: {
//   }
// })
