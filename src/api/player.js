import { get, post, destroy } from './base'
const GENERIC_ERROR_MESSAGE = 'Generic error message'
export const Player = {
  getAll: async () => {
    try {
      const response = await get('/api/Player')
      return response.data
    } catch (error) {
      const message = error?.response?.data?.message || error?.message
      throw new Error(`Error getAll: ${message}`)
    }
  },
  getPlayer: async ({ searchQuery }) => {
    try {
      const response = await get(`/api/Player/${searchQuery}`)
      return response.data
    } catch (error) {
      const message = error?.response?.data?.title || GENERIC_ERROR_MESSAGE
      console.error(message)
    }
  },
  storePlayer: async (data) => {
    try {
      const response = await post('/api/Player', data)
      return response.data
    } catch (error) {
      const message = error?.response?.data?.title || GENERIC_ERROR_MESSAGE
      console.error(message)
    }
  },
  deletePlayer: async ({ idPlayer }) => {
    try {
      await destroy(`/api/Player/${idPlayer}`)
    } catch (error) {
      const message = error?.response?.data?.title || GENERIC_ERROR_MESSAGE
      console.error(message)
    }
  }
}
